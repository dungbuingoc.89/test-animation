//
//  MovementSetting.swift
//  BahamaAirLoginScreen
//
//  Created by Sam on 12/6/19.
//  Copyright © 2019 Razeware LLC. All rights reserved.
//

import Foundation
import UIKit

public class MovemenSetting {
    enum BoundPosition {
        case corner
        case top
        case bottom
        case left
        case right
    }
    
    static func calculateViewBoundPosition(_ view: UIView) -> BoundPosition? {
        let screenSize: CGRect = UIScreen.main.bounds
        let viewOrigin = view.superview?.convert(view.frame.origin, to: nil)
        if (viewOrigin!.x <= CGFloat(0) && viewOrigin!.y <= CGFloat(0)) ||
            (viewOrigin!.x <= CGFloat(0) && viewOrigin!.y + view.frame.height >= screenSize.height) ||
            (viewOrigin!.x + view.frame.width >= screenSize.width && viewOrigin!.y <= CGFloat(0)) ||
            (viewOrigin!.x + view.frame.width >= screenSize.width && viewOrigin!.y + view.frame.height >= screenSize.height) {
            return .corner
        }
        if viewOrigin!.x <= 0 {
            return .left
        }
        if viewOrigin!.x + view.frame.width >= screenSize.width {
            return .right
        }
        if viewOrigin!.y <= 0 {
            return .top
        }
        if viewOrigin!.y + view.frame.height >= screenSize.height {
            return .bottom
        }
        return nil
    }
    
    static func calculateNewDirection(dX: CGFloat, dY: CGFloat, position: BoundPosition?) -> [CGFloat] {
        var result: [CGFloat] = []
        let standardX = CGFloat(2)
        let standardY = CGFloat(2)
        
        if let pos = position {
            var x = dX
            var y = dY
            switch pos {
            case .corner:
                x = -x * 1.45
                y = -y * 1.45
                break
            case .top, .bottom:
                x = x * 1.45
                y = -y * 1.45
                break
            case .left, .right:
                x = -x * 1.45
                y = y * 1.45
                break
            }
            if x >= 0 {
                x = x > 2*standardX ? 2*standardX : x
                x = x < standardX/2 ? standardX/2 : x
            } else {
                x = x < (-2)*standardX ? (-2)*standardX : x
                x = x > standardX/(-2) ? standardX/(-2) : x
            }
            
            if y >= 0 {
                y = y > 2*standardX ? 2*standardX : y
                y = y < standardX/2 ? standardX/2 : y
            } else {
                y = y < (-2)*standardY ? (-2)*standardY : y
                y = y > standardY/(-2) ? standardY/(-2) : y
            }
            
            result.append(x)
            result.append(y)
            return result
        }
        return [dX*0.9965, dY*0.9965]
    }
}
