/*
 * Copyright (c) 2014-present Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

// A delay function
func delay(_ seconds: Double, completion: @escaping ()->Void) {
  DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
}

class ViewController: UIViewController {
  
  // MARK: IB outlets
  
  @IBOutlet var loginButton: UIButton!
  @IBOutlet var heading: UILabel!
  @IBOutlet var username: UITextField!
  @IBOutlet var password: UITextField!
  
  @IBOutlet var cloud1: UIImageView!
  @IBOutlet var cloud2: UIImageView!
  @IBOutlet var cloud3: UIImageView!
  @IBOutlet var cloud4: UIImageView!
    @IBOutlet weak var bubble1: UIImageView!
    @IBOutlet weak var bubble2: UIImageView!
    @IBOutlet weak var leadingImageContraint: NSLayoutConstraint!
    @IBOutlet weak var bottomImageConstraint: NSLayoutConstraint!
    
  // MARK: further UI
  
  let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
  let status = UIImageView(image: UIImage(named: "banner"))
  let label = UILabel()
  let messages = ["Connecting ...", "Authorizing ...", "Sending credentials ...", "Failed"]
    
  
    var statusPosition = CGPoint.zero
    var dXBubble = CGFloat(2)
    var dYBubble = CGFloat(2)
    
    var dXBubble1 = CGFloat(-2)
    var dYBubble1 = CGFloat(2)
    
    var dXBubble2 = CGFloat(-2)
    var dYBubble2 = CGFloat(-2)
  // MARK: view controller methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //set up the UI
    loginButton.layer.cornerRadius = 8.0
    loginButton.layer.masksToBounds = true
    
    spinner.frame = CGRect(x: -20.0, y: 6.0, width: 20.0, height: 20.0)
    spinner.startAnimating()
    spinner.alpha = 0.0
    loginButton.addSubview(spinner)
    
    status.isHidden = true
    status.center = loginButton.center
    view.addSubview(status)
    
    label.frame = CGRect(x: 0.0, y: 0.0, width: status.frame.size.width, height: status.frame.size.height)
    label.font = UIFont(name: "HelveticaNeue", size: 18.0)
    label.textColor = UIColor(red: 0.89, green: 0.38, blue: 0.0, alpha: 1.0)
    label.textAlignment = .center
    status.addSubview(label)
    setupAnimation()
    snopDownAmimation()
    showAimataionBubble(bubble: self.bubble1, dx: self.dXBubble1, dy: self.dYBubble1)
    showAimataionBubble(bubble: self.bubble2, dx: self.dXBubble2, dy: self.dYBubble2)
    showAimataionBubble(bubble: self.cloud4, dx: self.dXBubble, dy: self.dYBubble)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    heading.center.x  -= view.bounds.width
    username.center.x -= view.bounds.width
    password.center.x -= view.bounds.width
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    UIView.animate(withDuration: 0.5) {
        self.heading.center.x += self.view.bounds.width
    }
    UIView.animate(withDuration: 0.6, delay: 0.3, options: [], animations: {
        self.username.center.x += self.view.bounds.width
    }, completion: nil)
    UIView.animate(withDuration: 0.7, delay: 0.4, options: [],
     animations: {
       self.password.center.x += self.view.bounds.width
     }, completion: nil
    )
  }
  
  // MARK: further methods
  
  @IBAction func login() {
//    view.endEditing(true)
//    self.changeIcon(to: "icon_app")
  }
  
  // MARK: UITextFieldDelegate
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    let nextField = (textField === username) ? password : username
    nextField?.becomeFirstResponder()
    return true
  }
  
    func changeIcon(to name: String?) {
        //Check if the app supports alternating icons
        if #available(iOS 10.3, *) {
            guard UIApplication.shared.supportsAlternateIcons else {
                return;
            }
        } else {
            // Fallback on earlier versions
        }
        
        //Change the icon to a specific image with given name
        if #available(iOS 10.3, *) {
            UIApplication.shared.setAlternateIconName(name) { (error) in
                //After app icon changed, print our error or success message
                if let error = error {
                    print("App icon failed to due to \(error.localizedDescription)")
                } else {
                    print("App icon changed successfully.")
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setupAnimation() {
        let options: UIView.AnimationOptions = [.curveEaseInOut,
                                                .repeat,
                                                .autoreverse]
        
        UIView.animate(withDuration: 2.9,
                       delay: 0,
                       options: options,
                       animations: { [weak self] in
                        self?.cloud1.frame.size.height *= 1.18
                        self?.cloud1.frame.size.width *= 1.18
        }, completion: nil)
        
        UIView.animate(withDuration: 3.0,
                       delay: 0.2,
                       options: options,
                       animations: { [weak self] in
                        self?.cloud2.frame.size.height *= 1.28
                        self?.cloud2.frame.size.width *= 1.28
        }, completion: nil)
        
        UIView.animate(withDuration: 0.7,
                       delay: 0.1,
                       options: options,
                       animations: { [weak self] in
                        self?.cloud3.frame.size.height *= 1.15
                        self?.cloud3.frame.size.width *= 1.15
        }, completion: nil)
        
//        UIView.animate(withDuration: 1,
//                       delay: 0.5,
//                       options: options,
//                       animations: { [weak self] in
////                        self?.cloud4.frame.size.height *= 1.23
////                        self?.cloud4.frame.size.width *= 1.23
//                        let randomInt = CGFloat.random(in: 0..<50)
////                        self?.leadingImageContraint.constant += randomInt
////                        self?.bottomImageConstraint.constant += randomInt
//                        self?.cloud4.frame = CGRect(x: randomInt, y: randomInt, width: 100, height: 100)
//        }, completion: nil)
        
//        let originalTransform = self.cloud4.transform
//        let scaledTransform = originalTransform.scaledBy(x: 0.25, y: 0.25)
//        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
//        UIView.animate(withDuration: 0.7, delay: 0.5, options: options, animations: {
//            self.cloud4.transform = scaledAndTranslatedTransform
//        }, completion: nil)
        
        UIView.animate(withDuration: 1,
                     delay: 1.2,
                     options: .curveEaseInOut,
                     animations: { [weak self] in
                      self?.loginButton.backgroundColor = .systemYellow
        }, completion: nil)
    }
    
    func showAimataionBubble(bubble: UIImageView, dx: CGFloat, dy: CGFloat) {
        var x = dx
        var y = dy
        let currentPos = MovemenSetting.calculateViewBoundPosition(bubble)
        let newPos = MovemenSetting.calculateNewDirection(dX: x, dY: y, position: currentPos)
        x = newPos.first!
        y = newPos.last!
        UIView.animate(withDuration: 0.01,
                       delay: 0,
                       options: [.curveLinear],
                       animations: {
                        bubble.frame = bubble.frame.offsetBy(dx: x, dy: y)
        }, completion: { finish in
            self.showAimataionBubble(bubble: bubble, dx: x, dy: y)
        })
    }
    
    func snopDownAmimation() {
        let flakeEmitterCell = CAEmitterCell()
        flakeEmitterCell.contents = UIImage(named: "snowFlake")?.cgImage
        flakeEmitterCell.scale = 0.06
        flakeEmitterCell.scaleRange = 0.3
        flakeEmitterCell.emissionRange = .pi
        flakeEmitterCell.lifetime = 20.0
        flakeEmitterCell.birthRate = 40
        flakeEmitterCell.velocity = -30
        flakeEmitterCell.velocityRange = -20
        flakeEmitterCell.yAcceleration = 30
        flakeEmitterCell.xAcceleration = 5
        flakeEmitterCell.spin = -0.5
        flakeEmitterCell.spinRange = 1.0

        let snowEmitterLayer = CAEmitterLayer()
        snowEmitterLayer.emitterPosition = CGPoint(x: view.bounds.width / 2.0, y: -50)
        snowEmitterLayer.emitterSize = CGSize(width: view.bounds.width, height: 0)
        snowEmitterLayer.emitterShape = kCAEmitterLayerLine
        snowEmitterLayer.beginTime = CACurrentMediaTime()
        snowEmitterLayer.timeOffset = 10
        snowEmitterLayer.emitterCells = [flakeEmitterCell]
        
        self.view.layer.addSublayer(snowEmitterLayer)
    }
    
}
